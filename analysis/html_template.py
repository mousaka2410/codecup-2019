html_template = """
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Flippo game</title>

<script type="text/javascript">
var moves =
{moves};
</script><script type="text/javascript" src="../flippo/bin/resources/flippo.js"></script>
<link rel="stylesheet" type="text/css" href="../flippo/bin/resources/flippo.css">
</head>
<body>
<h2>Flippo game /home/stian/caia/flippo/refereelogs/dantes-player3.31.log</h2>

<table><tbody>
<tr>
<th align="left">Wit:&nbsp;&nbsp;</th>
<td class="kimborder">White</td>
</tr>
<tr>
<th align="left">Zwart:&nbsp;&nbsp;</th>
<td class="kimborder">Black</td>
</tr>
<tr><th align="left">Message:&nbsp;&nbsp;</th><td class="kimborder">PLAYER_1:_DRAW;_PLAYER_2:_DRAW</td></tr>
<tr><th align="left">Result:&nbsp;&nbsp;</th><td class="kimborder">30 - 30<br></td></tr>
</tbody></table>

<div id="optional-moves">
{links}
</div>

<div id="game-report">
<h3>Board position</h3>

<div id="game-container" class="show-flips"></div>
<div class="button-bar">
<button id="first">&#8810;</button>
<button id="prev">&lt;</button>
<button id="next">&gt;</button>
<button id="last">&#8811;</button>
<button id="play-pause">Play</button>
<input checked type="checkbox" id="show-flips" /><label for="show-flips">Show flips</label></div>
<div id="move-table" tabindex="1"></div>
<script type="application/javascript">
var name1 = 'White', name2 = 'Black';
var names = [ name1, name2 ];
var f = new Flippo(names, moves);
f.setMove({start_pos});
</script>
<p>&copy; 2018-2019 CodeCup, Ludo Pulles</p>
</div>
</body></html>
"""

link_template = '<a href="{file}"> {score} {moves} </a><br/>\n'
