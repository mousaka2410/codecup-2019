#include "analysis.hpp"

#include "alphabeta.hpp"

std::vector<Move> getBestPlayout(Position position) {
    int movesPlayed = popcnt(position.current | position.opponent);
    int movesLeft = 64 - movesPlayed;
    std::vector<Move> res;
    res.reserve(movesLeft);
    for (int depth = movesLeft; depth > 0; depth--) {
        res.push_back(alphabeta(position, depth));
        applyMoveAndSwap(position, res.back());
    }
    return res;
}

void playoutMoves(Position & position, const std::vector<Move> & moves) {
    for (const Move & move : moves) {
        applyMoveAndSwap(position, move);
    }
}
