#include "analysis.hpp"
#include "move.hpp"
#include "input.hpp"

#include <nlohmann/json.hpp>
using json = nlohmann::json;

#include <iostream>
#include <iomanip>
#include <array>
#include <cassert>

int endPositionScore(const Position & position, bool isStarting) {
    int score = scoreForCurrent(position);
    if (!isStarting)
        score = 60 - score;
    return score;
}

std::ostream & printMoves(std::ostream & stream, const std::vector<Move> & moves) {
    std::string delim = "";
    for (const Move & move : moves) {
        stream << delim << moveToCoords(move.move);
        delim = ", ";
    }
    return stream;
}

void to_json(json & j, const Move & move) {
    j = moveToCoords(move.move);
}

int main() {
    int minDepth = 1;
    int maxDepth = 10;
    bool isStartingPlayer = isStarting(std::cin);

    json analysis;

    std::vector<Position> positions;
    positions.reserve(61);
    positions.emplace_back(startPosWhite, startPosBlack);
    while (positions.size() < 61) {
        std::string coords;
        std::cin >> coords;
        assert(std::cin);
        analysis["actualGame"].push_back(coords);
        Move move = getMove(positions.back(), coordsToMove(coords));
        positions.push_back(positions.back());
        applyMoveAndSwap(positions.back(), move);
    };

    int realScore = endPositionScore(positions.back(), isStartingPlayer);

    analysis["whiteScore"] = isStartingPlayer ? realScore : 60 - realScore;
    analysis["blackScore"] = isStartingPlayer ? 60 - realScore : realScore;
    analysis["yourColor"] = isStartingPlayer ? "white" : "black";
    analysis["yourScore"] = realScore;

    int depth = minDepth + (isStartingPlayer ? (minDepth & 1) : !(minDepth & 1));

    for (; depth <= maxDepth; depth += 2) {
        Position branchPosition = positions[60 - depth];
        std::vector<Move> moves = getBestPlayout(branchPosition);
        playoutMoves(branchPosition, moves);
        int optimalScore = endPositionScore(branchPosition, isStartingPlayer);
        analysis["optionalPlayouts"].push_back(
            json {
                { "depth", depth },
                { "score", optimalScore },
                { "moves", moves }
            }
        );
    }
    std::cout << std::setw(4) << analysis << std::endl;
}
