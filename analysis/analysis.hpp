#ifndef ANALYSIS_HPP
#define ANALYSIS_CPP

#include "move.hpp"
#include <vector>

std::vector<Move> getBestPlayout(Position position);
void playoutMoves(Position & position, const std::vector<Move> & moves);

#endif // ANALYSIS_HPP
