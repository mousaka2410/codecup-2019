#!/usr/bin/python3

from pathlib import Path
from subprocess import Popen, PIPE

def visit_logs(callback):
    command_extract_moves = "./analysis_scripts/both_players_moves.sh"
    command_best_playouts = "./analysis/best_playouts"

    logdir = Path('./flippo/playerlogs')

    for logfile in logdir.iterdir():
        if not logfile.is_file():
            continue
        filename_components = logfile.name.split('.')
        if filename_components[0] != 'dantes':
            continue

        with open(logfile) as infile:
            extract_moves_process = Popen([command_extract_moves], stdout=PIPE, stdin=infile)
            best_playouts_process = Popen([command_best_playouts], stdin=extract_moves_process.stdout, stdout=PIPE)
            extract_moves_process.stdout.close()
            out, err = best_playouts_process.communicate();
            callback(out.decode('UTF-8'), filename_components)
        

if __name__ == "__main__":
    visit_logs(print)
