#include "bitutil.hpp"
#include <iostream>

int main() {
    ull pos;
    while (std::cin >> pos, pos) {
        printBitMatrix(std::cout, pos);
    }
}

