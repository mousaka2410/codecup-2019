#!/usr/bin/python3

import generate_analysis
import json
from html_template import html_template, link_template
from pathlib import Path


def generate_html_from_analysis(analysis_str, filename_components):
    analysis = json.loads(analysis_str)

    path = Path.cwd() / 'playouts'
    path.mkdir(exist_ok=True)

    filename_components[-1] = "html"
    filename_components.insert(-1, "0")

    file_path = (path / '.'.join(filename_components)).resolve()

    modified_filename_components = filename_components.copy()

    links = link_template.format(score=analysis['yourScore'], moves="actual game", file=file_path)
    for playout in analysis['optionalPlayouts']:
        modified_filename_components[-2] = str(playout['score'])
        modified_file_path = (path / '.'.join(modified_filename_components)).resolve()
        links += link_template.format(score=playout['score'], moves=playout['moves'], file=modified_file_path)
    
    with open(file_path, 'w') as html_file:
        html_file.write(html_template.format(moves=analysis['actualGame'], links=links, start_pos=0))
    
    for playout in analysis['optionalPlayouts']:
        modified_filename_components[-2] = str(playout['score'])
        modified_file_path = (path / '.'.join(modified_filename_components)).resolve()
        depth = playout['depth']
        moves = analysis['actualGame'][:-depth] + playout['moves']
        with open(modified_file_path, 'w') as html_file:
            html_file.write(html_template.format(moves=moves, links=links, start_pos=60-depth))
    
    print('file://' + file_path.as_posix())


def main():
    generate_analysis.visit_logs(generate_html_from_analysis)

if __name__ == '__main__':
    main()
