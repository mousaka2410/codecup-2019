#include "move.hpp"

#include "bitutil.hpp"
#include "movemasks.hpp"

int rotatePosRight(int pos) {
    int x = pos % 8;
    int y = pos / 8;
    return x*8 + (7-y);
}

int pointMirrorPos(int pos) {
    return 63 - pos;
}

int rotatePosLeft(int pos) {
    int x = pos % 8;
    int y = pos / 8;
    return (7-x)*8 + y;
}

constexpr ull rightLimitedMask = "11111111"
                                 "00000000"
                                 "11111111"
                                 "00000000"
                                 "00000000"
                                 "00000000"
                                 "00000000"
                                 "11111111"_lsb_ull;

constexpr ull leftLimitedMask = "00000000"
                                "00000000"
                                "00000000"
                                "11111111"
                                "11111111"
                                "00000000"
                                "11111111"
                                "00000000"_lsb_ull;

constexpr ull upLimitedMask = "00000000"
                              "00000000"
                              "00000000"
                              "00000000"
                              "00000000"
                              "11111111"
                              "11111111"
                              "11111111"_lsb_ull;

constexpr ull downLimitedMask = "00000000"
                                "11111111"
                                "11111111"
                                "11111111"
                                "00000000"
                                "00000000"
                                "00000000"
                                "00000000"_lsb_ull;

constexpr ull secondColumnMask = "01000000"
                                 "01000000"
                                 "01000000"
                                 "01000000"
                                 "01000000"
                                 "01000000"
                                 "01000000"
                                 "01000000"_lsb_ull;

ull moveFlipMaskV2(ull currentPlayersPieces, ull opponentsPieces, int pos) {
    ull current  = currentPlayersPieces;
    ull opponent = opponentsPieces;
    current  = rotateBitsRight(current, pos);
    opponent = rotateBitsRight(opponent, pos);

    const ull leftPart = rightZero[pos % 8];

    current  = leftRot8(current  & ~leftPart) | (current  & leftPart);
    ull currentMajor  = (current  & majorDiagonalRayMask) * 0x7F;
    ull currentMinor  = (current  & minorDiagonalRayMask) * 0x7F;

    opponent = leftRot8(opponent & ~leftPart) | (opponent & leftPart);
    ull opponentMajor = (opponent & majorDiagonalRayMask) * 0x7F;
    ull opponentMinor = (opponent & minorDiagonalRayMask) * 0x7F;

    ull toTransPose = (current & leftBitsMask) << 1 |
                      ((currentMajor & rightBitsMask) >> 5) |
                      ((currentMinor & rightBitsMask) >> 4) |
                      ((opponent & leftBitsMask) << 5) |
                      ((opponentMajor & rightBitsMask) >> 1) |
                      (opponentMinor & rightBitsMask);

    ull transposed = transposeBitMatrix(toTransPose);

    transposed |= (current & upperBitsMask1) | ((opponent & upperBitsMask1) << 32);

    ull  flipped = horizontalFlipBitMatrix(transposed) << 1;

    constexpr ull upperHalf = upperOne[4];
    constexpr ull lowerHalf = ~upperHalf;

    current  = (transposed & upperHalf) | ((flipped & upperHalf) << 32);
    opponent = ((transposed & lowerHalf) >> 32) | (flipped & lowerHalf);

    ull hasPiece  = current | opponent;

    int leftLimit = pos % 8;
    int upLimit = pos / 8;
    int rightLimit = 7 - leftLimit;
    int downLimit = 7 - upLimit;
    ull limitMask = (rightLimitedMask & (secondColumnMask << rightLimit)) |
                    (leftLimitedMask  & (secondColumnMask << leftLimit)) |
                    (upLimitedMask    & (secondColumnMask << upLimit)) |
                    (downLimitedMask  & (secondColumnMask << downLimit));

    hasPiece &= ~limitMask;

    ull longestRays = hasPiece & ~(hasPiece + secondColumnMask);
    ull currentsPiecesInLongestRays = longestRays & current;
    ull toFlip = currentsPiecesInLongestRays | (currentsPiecesInLongestRays >> 1);
    toFlip = toFlip | ((toFlip & leftZero[2]) >> 2);
    toFlip = toFlip | ((toFlip & leftZero[4]) >> 4);
    toFlip = (toFlip & leftZero[1]) >> 1;
    toFlip &= leftZero[1];

    ull backFlipped = horizontalFlipBitMatrix(toFlip >> 1);
    toFlip |= backFlipped >> 32;
    ull result = toFlip & upperBitsMask1;
    toFlip = transposeBitMatrix(toFlip);
    ull flipMajor = (((toFlip >> 1) & secondColumnMask) * 0x7F) & majorDiagonalRayMask;
    ull flipMinor = (((toFlip >> 2) & secondColumnMask) * 0x7F) & minorDiagonalRayMask;
    result |= flipMajor | flipMinor | ((toFlip & secondColumnMask) >> 1);

    result = rotateBitsRight(result & ~leftPart, 8) | (result & leftPart);
    result = rotateBitsLeft(result, pos);

    return result;
}

ull moveFlipMask(ull currentPlayersPieces, ull opponentsPieces, int pos) {
    int posShift0 = pos;
    int posShift1 = rotatePosRight(pos);
    int posShift2 = pointMirrorPos(pos);
    int posShift3 = rotatePosLeft(pos);

    ull current = currentPlayersPieces;
    ull current0 = ((current                       & leftZero[posShift0 % 8]) >> posShift0) & rayMask;
    ull current1 = ((rotateBitMatrixRight(current) & leftZero[posShift1 % 8]) >> posShift1) & rayMask;
    ull current2 = ((pointMirrorBitMatrix(current) & leftZero[posShift2 % 8]) >> posShift2) & rayMask;
    ull current3 = ((rotateBitMatrixLeft(current)  & leftZero[posShift3 % 8]) >> posShift3) & rayMask;

    ull opponent = opponentsPieces;
    ull opponent0 = ((opponent                       & leftZero[posShift0 % 8]) >> posShift0) & rayMask;
    ull opponent1 = ((rotateBitMatrixRight(opponent) & leftZero[posShift1 % 8]) >> posShift1) & rayMask;
    ull opponent2 = ((pointMirrorBitMatrix(opponent) & leftZero[posShift2 % 8]) >> posShift2) & rayMask;
    ull opponent3 = ((rotateBitMatrixLeft(opponent)  & leftZero[posShift3 % 8]) >> posShift3) & rayMask;

    current0 = (((current0 >> 8) * leftBitsMask) << 8) | current0;
    current1 = (((current1 >> 8) * leftBitsMask) << 8) | current1;
    current2 = (((current2 >> 8) * leftBitsMask) << 8) | current2;
    current3 = (((current3 >> 8) * leftBitsMask) << 8) | current3;

    opponent0 = (((opponent0 >> 8) * leftBitsMask) << 8) | opponent0;
    opponent1 = (((opponent1 >> 8) * leftBitsMask) << 8) | opponent1;
    opponent2 = (((opponent2 >> 8) * leftBitsMask) << 8) | opponent2;
    opponent3 = (((opponent3 >> 8) * leftBitsMask) << 8) | opponent3;

    current0 = leftRot8(current0) & upperBitsMask2;
    current1 = leftRot8(current1) & upperBitsMask2;
    current2 = leftRot8(current2) & upperBitsMask2;
    current3 = leftRot8(current3) & upperBitsMask2;

    opponent0 = leftRot8(opponent0) & upperBitsMask2;
    opponent1 = leftRot8(opponent1) & upperBitsMask2;
    opponent2 = leftRot8(opponent2) & upperBitsMask2;
    opponent3 = leftRot8(opponent3) & upperBitsMask2;

    current  = current0  | (current1  << 16) | (current2  << 32) | (current3  << 48);
    opponent = opponent0 | (opponent1 << 16) | (opponent2 << 32) | (opponent3 << 48);
    
    ull hasPiece = current | opponent;
    ull longestRays = hasPiece & ~(hasPiece + (leftBitsMask << 1));
    ull currentsPiecesInLongestRays = longestRays & current;
    ull toFlip = currentsPiecesInLongestRays | (currentsPiecesInLongestRays >> 1);
    toFlip = toFlip | ((toFlip & leftZero[2]) >> 2);
    toFlip = toFlip | ((toFlip & leftZero[4]) >> 4);
    toFlip = (toFlip & leftZero[1]) >> 1;
    
    ull toFlip0 = ((((toFlip       & upperBitsMask1) << 8) * leftBitsMask) | ((toFlip >>  8) & upperBitsMask1)) & rayMask;
    ull toFlip1 = ((((toFlip >> 16 & upperBitsMask1) << 8) * leftBitsMask) | ((toFlip >> 24) & upperBitsMask1)) & rayMask;
    ull toFlip2 = ((((toFlip >> 32 & upperBitsMask1) << 8) * leftBitsMask) | ((toFlip >> 40) & upperBitsMask1)) & rayMask;
    ull toFlip3 = ((((toFlip >> 48 & upperBitsMask1) << 8) * leftBitsMask) | ((toFlip >> 56)                 )) & rayMask;

    toFlip0 = toFlip0 << posShift0;
    toFlip1 = rotateBitMatrixLeft(toFlip1 << posShift1);
    toFlip2 = pointMirrorBitMatrix(toFlip2 << posShift2);
    toFlip3 = rotateBitMatrixRight(toFlip3 << posShift3);

    return toFlip0 | toFlip1 | toFlip2 | toFlip3;
}

ull subFlipMask(ull allPieces, ull currentPlayersPieces, int pos, int dpos) {
    static constexpr bool (*horizontalEdgeChecks[])(int) = {
        static_cast<bool (*)(int)>([](int currentPos) { return currentPos % 8 == 0; }),
        static_cast<bool (*)(int)>([](int)            { return false; }),
        static_cast<bool (*)(int)>([](int currentPos) { return currentPos % 8 == 7; }),
    };
    auto horizontalEdgeCheck = horizontalEdgeChecks[(dpos + 9) % 8];
    auto verticalEdgeCheck = [](int nextPos) { return nextPos < 0 || 63 < nextPos; };

    ull res = 0ULL;
    ull spekulativeRes = 0ULL;

    for (int nextPos = pos + dpos; !verticalEdgeCheck(nextPos) && !horizontalEdgeCheck(pos) && ((allPieces >> nextPos) & 1); pos = nextPos, nextPos += dpos) {
        if ((currentPlayersPieces >> nextPos) & 1)
            res = spekulativeRes;
        
        spekulativeRes |= (1ULL << nextPos);
    }
    return res;
}

ull moveFlipMask_branched(ull currentPlayersPieces, ull opponentsPieces, int pos) {
    ull allPieces = currentPlayersPieces | opponentsPieces;
    ull res = 0ULL;
    constexpr int dpos[8] = {-9, -8, -7, -1, 1,  7, 8, 9 };
    for (std::size_t i = 0; i < 8; ++i) {
        res |= subFlipMask(allPieces, currentPlayersPieces, pos, dpos[i]);
    }
    return res;
}

ull adjacentsMask(ull currentPlayersPieces, ull opponentsPieces) {
    ull allPieces = currentPlayersPieces | opponentsPieces;
    ull result = allPieces | ((allPieces & leftZero[1]) >> 1) | ((allPieces << 1) & leftZero[1]);
    result |= (result << 8) | (result >> 8);
    return result ^ allPieces;
}

ull possibleMovesMask(ull currentPlayersPieces, ull opponentsPieces) {
    ull allPieces = currentPlayersPieces | opponentsPieces;

    ull result = 0ULL;

    // left
    ull mask1 = (allPieces & leftZero[1]) >> 1;
    ull mask2 = (allPieces & leftZero[2]) >> 2;

    ull left = mask1 & mask2;
    ull up   = (mask1 >> 8) & (mask2 >> 16);
    ull down = (mask1 << 8) & (mask2 << 16);

    result = left | up | down;

    // up/down
    up   = (allPieces >> 8) & (allPieces >> 16);
    down = (allPieces << 8) & (allPieces << 16);

    result |= up | down;

    // right
    mask1 = (allPieces << 1) & leftZero[1];
    mask2 = (allPieces << 2) & leftZero[2];

    ull right = mask1 & mask2;
    up    = (mask1 >> 8) & (mask2 >> 16);
    down  = (mask1 << 8) & (mask2 << 16);

    result |= right | up | down;

    return result & ~allPieces;
}
