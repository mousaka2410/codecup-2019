#include "greedy.hpp"
#include "move.hpp"
#include <iostream>
#include <string>
#include <algorithm>
#include <chrono>

int coordsToMove(const std::string & coords) {
    return (coords[0] - 'A') * 8 + (coords[1] - '1');
}

std::string moveToCoords(int move) {
    std::string res = "..";
    res[0] = (move / 8) + 'A';
    res[1] = (move % 8) + '1';
    return res;
}

int main() {
    std::ios::sync_with_stdio(false);

    std::string input;
    std::cin >> input;
    std::cerr << "In: " << input << '\n';

    ull me = startPosWhite;
    ull opponent = startPosBlack;

    int moveNo = 0;

    if (input != "Start") {
        std::swap(me, opponent);
        int move = coordsToMove(input);
        ull flipMask = moveFlipMaskV2(opponent, me, move);
        applyFlip(opponent, me, flipMask);
        addPiece(opponent, move);
        moveNo++;
    }

    {
        std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
        MoveStats move = superGreedy(me, opponent);
        applyFlip(me, opponent, move.flipMask);
        addPiece(me, move.move);
        std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
        std::string coords = moveToCoords(move.move);
        std::cout << coords << std::endl;
        std::cerr << "Out: " << coords << '\n';
        std::cerr << "Time: " << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << "us\n";
        moveNo++;
    }

    for (; moveNo < 60; moveNo += 2) {
        {
            std::cin >> input;
            std::cerr << "In: " << input << '\n';
            int move = coordsToMove(input);
            ull flipMask = moveFlipMaskV2(opponent, me, move);
            applyFlip(opponent, me, flipMask);
            addPiece(opponent, move);
        }

        {
            std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
            MoveStats move = superGreedy(me, opponent);
            applyFlip(me, opponent, move.flipMask);
            addPiece(me, move.move);
            std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
            std::string coords = moveToCoords(move.move);
            std::cout << coords << std::endl;
            std::cerr << "Out: " << coords << '\n';
            std::cerr << "Time: " << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << "us\n";
        }
    }

    return 0;
}