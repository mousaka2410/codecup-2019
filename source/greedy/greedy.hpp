#ifndef GREEDY_HPP
#define GREEDY_HPP

#include "bitutil.hpp"
#include "move.hpp"

using MoveStats = Move;
MoveStats greedy(ull currentPlayersPieces, ull opponentsPieces);
MoveStats superGreedy(ull currentPlayersPieces, ull opponentsPieces);

#endif
