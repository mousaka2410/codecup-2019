#include "greedy.hpp"
#include "move.hpp"

#include <algorithm>

void findGreediestMove(MoveStats & currentBest, ull currentPlayersPieces, ull opponentsPieces, int move) {
    ull flipMask = moveFlipMaskV2(currentPlayersPieces, opponentsPieces, move);
    int nCurrentsPieces = flipMask 
                          ? popcnt(currentPlayersPieces ^ flipMask) + 1
                          : 0;
    if (currentBest.score < nCurrentsPieces) {
        currentBest.move = move;
        currentBest.score = nCurrentsPieces;
        currentBest.flipMask = flipMask;
    }
}

MoveStats greedy(ull currentPlayersPieces, ull opponentsPieces) {
    MoveStats move;
    visitPossibleMoves<MoveStats, findGreediestMove>(move, currentPlayersPieces, opponentsPieces);
    return move;
}

void findSuperGreediestMove(MoveStats & currentBest, ull currentPlayersPieces, ull opponentsPieces, int move) {
    ull current = currentPlayersPieces;
    ull opponent = opponentsPieces;

    ull thisMoveFlipMask = moveFlipMaskV2(current, opponent, move);
    if (!thisMoveFlipMask) {
        if (currentBest.score < 0) {
            currentBest.score = 0;
            currentBest.move = move;
            currentBest.flipMask = thisMoveFlipMask;
        }
        return;
    }

    applyFlip(current, opponent, thisMoveFlipMask);
    addPiece(current, move);
    ull allPieces = current | opponent;

    int nSwaps = 0;

    while (~allPieces) {
        std::swap(current, opponent);
        nSwaps++;
        int nextMove = greedy(current, opponent).move;
        ull flipMask = moveFlipMaskV2(current, opponent, nextMove);
        applyFlip(current, opponent, flipMask);
        addPiece(current, nextMove);
        allPieces = current | opponent;
    }

    if (nSwaps & 1)
        std::swap(current, opponent);
        
    int nCurrentsPieces = popcnt(current);

    if (currentBest.score < nCurrentsPieces) {
        currentBest.move = move;
        currentBest.score = nCurrentsPieces;
        currentBest.flipMask = thisMoveFlipMask;
    }
}

MoveStats superGreedy(ull currentPlayersPieces, ull opponentsPieces) {
    MoveStats move;
    visitPossibleMoves<MoveStats, findSuperGreediestMove>(move, currentPlayersPieces, opponentsPieces);
    return move;
}

