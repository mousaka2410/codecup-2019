#include "input.hpp"

#include <istream>

int coordsToMove(const std::string & coords) {
    return (coords[0] - 'A') * 8 + (coords[1] - '1');
}

std::string moveToCoords(int move) {
    std::string res = "..";
    res[0] = (move / 8) + 'A';
    res[1] = (move % 8) + '1';
    return res;
}

bool isStarting(std::istream & stream) {
    bool starting = false;
    if (stream.peek() == 'S') {
        starting = true;
        std::string startToken;
        stream >> startToken;
    }
    return starting;
}
