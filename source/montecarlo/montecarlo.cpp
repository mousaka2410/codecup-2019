#include "montecarlo.hpp"
#include "move.hpp"

#include <vector>
#include <map>
#include <numeric>

namespace {
int getRand(RandomContext &, int upperLimit) {
    return rand() % upperLimit;
}
}

struct MonteCarloNode {
    int nVisited = 0;
    int score = 30;
    ull currentPieces;
    ull opponentPieces;
    ull flipMask;
    std::vector<int> possibleMoves;
    std::map<int, MonteCarloNode> expandedNodes;

    MonteCarloNode(ull current, ull opponent, ull flipMask);
};

MonteCarloNode & expandNode(MonteCarloNode & node, int idx) {
    Position position(node.currentPieces, node.opponentPieces);
    int pos = node.possibleMoves[idx];

    Move move = getMove(position, pos);
    applyMoveAndSwap(position, move);

    MonteCarloNode & newNode =
        node.expandedNodes.emplace(std::piecewise_construct,
            std::forward_as_tuple(idx),
            std::forward_as_tuple(position.current, position.opponent, move.flipMask)).first->second;

    return newNode;
}

struct PossibleMoves {
    std::vector<int> possibleMoves;
    bool hasFlipMoves = false;
};

void addPossibleToVector(PossibleMoves & possible, ull current, ull opponent, int pos) {
    bool isFlipMove = 0ULL != moveFlipMaskV2(current, opponent, pos);
    if (!possible.hasFlipMoves && isFlipMove) {
        possible.hasFlipMoves = true;
        possible.possibleMoves.clear();
    }
    if (!possible.hasFlipMoves || isFlipMove) {
        possible.possibleMoves.push_back(pos);
    }
}

MonteCarloNode::MonteCarloNode(ull current, ull opponent, ull flipMask)
    : currentPieces(current)
    , opponentPieces(opponent)
    , flipMask(flipMask)
{
    PossibleMoves possible;
    visitPossibleMoves<PossibleMoves, addPossibleToVector>(possible, current, opponent);
    this->possibleMoves = std::move(possible.possibleMoves);
}

class MonteCarloImpl {
public:
    MonteCarloImpl(RandomContext & randomContext);
    Move bestMove(ull current, ull opponent, int moveNo);
private:
    RandomContext & randomContext_;
};

MonteCarloImpl::MonteCarloImpl(RandomContext & randomContext) 
    : randomContext_(randomContext) 
{ }

int selectMove(const MonteCarloNode & node, RandomContext & randomContext) {
    std::vector<int> probableScores(node.possibleMoves.size(), 20);
    for (const auto & [idx, nextNode] : node.expandedNodes) {
        probableScores[idx] = nextNode.score / nextNode.nVisited;
    }
    std::partial_sum(probableScores.begin(), probableScores.end(), probableScores.begin());
    int upperLimit = probableScores.back();

    int roll = getRand(randomContext, upperLimit);

    int idx = std::upper_bound(probableScores.cbegin(), probableScores.cend(), roll) - probableScores.cbegin();
    return idx;
}

int performPlayout(MonteCarloNode & node, RandomContext & randomContext, int moveNo) {
    Position position(node.currentPieces, node.opponentPieces);

    for (int i = moveNo; i < 60; i++) {
        PossibleMoves possible;
        visitPossibleMoves<PossibleMoves, addPossibleToVector>(possible, position.current, position.opponent);
        int moveIdx = getRand(randomContext, possible.possibleMoves.size());
        Move move = getMove(position, possible.possibleMoves[moveIdx]);
        applyMoveAndSwap(position, move);
    }

    int score = scoreForCurrent(position);
    return (moveNo & 1) ? score : 60 - score;
}

void applyScore(std::vector<MonteCarloNode*> branch, int score) {
    for (auto it = branch.rbegin(); it != branch.rend(); ++it) {
        (*it)->score += score;
        (*it)->nVisited++;
        score = 60 - score;
    }
}
Move MonteCarloImpl::bestMove(ull current, ull opponent, int moveNo) {
    MonteCarloNode root(current, opponent, 0ULL);
    for (int i = 0; i < 4400; i++) {
        MonteCarloNode * node = &root;
        std::vector<MonteCarloNode*> currentBranch;
        currentBranch.reserve(60 - moveNo);
        std::map<int, MonteCarloNode>::iterator nodeIt;
        int idx = 0;
        do {
            currentBranch.push_back(node);
            if (node->possibleMoves.empty())
                break;
            idx = selectMove(*node, randomContext_);
            nodeIt = node->expandedNodes.find(idx);
        } while (nodeIt != node->expandedNodes.end() && (node = &nodeIt->second));

        if (!node->possibleMoves.empty()) {
            node = &expandNode(*node, idx);
            currentBranch.push_back(node);
        }

        int score = performPlayout(*node, randomContext_, moveNo + currentBranch.size());
        applyScore(currentBranch, score);
    }

    ull bestFlipMask = 0ULL;
    int bestScore = 0;
    int bestIdx = -1;
    for (const auto & [idx, node] : root.expandedNodes) {
        int score = node.score / node.nVisited;
        if (score > bestScore) {
            bestFlipMask = node.flipMask;
            bestScore = score;
            bestIdx = idx;
        }
    }
    return Move(root.possibleMoves[bestIdx], bestFlipMask, bestScore);
}

MonteCarlo::MonteCarlo(RandomContext & randomContext) 
    : impl_(std::make_unique<MonteCarloImpl>(randomContext))
{ ; }

MonteCarlo::~MonteCarlo() = default;

Move MonteCarlo::bestMove(ull current, ull opponent, int moveNo) {
    return impl_->bestMove(current, opponent, moveNo);
}
