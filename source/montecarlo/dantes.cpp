#include "montecarlo.hpp"
#include "move.hpp"
#include "input.hpp"
#include <iostream>
#include <string>
#include <algorithm>
#include <chrono>


struct RandomContext {
    int nCalls;
};

int main() {
    std::ios::sync_with_stdio(false);

    time_t seed = time(NULL);
    // seed = 1546213778;
    std::cerr << "Seed: " << seed << '\n';
    srand(seed);

    std::string input;
    std::cin >> input;
    std::cerr << "In: " << input << '\n';

    ull me = startPosWhite;
    ull opponent = startPosBlack;

    int moveNo = 0;
    RandomContext randomContext;
    MonteCarlo monteCarlo(randomContext);

    using namespace std::literals::chrono_literals;

    std::chrono::milliseconds lapTime;
    std::chrono::milliseconds totalTime = 0ms;

    if (input != "Start") {
        std::swap(me, opponent);
        int move = coordsToMove(input);
        ull flipMask = moveFlipMaskV2(opponent, me, move);
        applyFlip(opponent, me, flipMask);
        addPiece(opponent, move);
        moveNo++;
    }

    {
        std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
        Move move = monteCarlo.bestMove(me, opponent, moveNo);
        applyFlip(me, opponent, move.flipMask);
        addPiece(me, move.move);
        std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
        std::string coords = moveToCoords(move.move);
        std::cerr << "Out: " << coords << '\n';
        std::cerr << "Score: " << move.score << '\n';
        lapTime = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
        totalTime += lapTime;
        std::cerr << "Time: " << lapTime.count() << "ms / " << totalTime.count() << "ms\n";
        std::cout << coords << std::endl;
        moveNo++;
    }

    for (; moveNo < 60; moveNo += 2) {
        {
            std::cin >> input;
            std::cerr << "In: " << input << '\n';
            int move = coordsToMove(input);
            ull flipMask = moveFlipMaskV2(opponent, me, move);
            applyFlip(opponent, me, flipMask);
            addPiece(opponent, move);
        }

        {
            std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
            Move move = monteCarlo.bestMove(me, opponent, moveNo);
            applyFlip(me, opponent, move.flipMask);
            addPiece(me, move.move);
            std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
            std::string coords = moveToCoords(move.move);
            std::cerr << "Out: " << coords << '\n';
            std::cerr << "Score: " << move.score << '\n';
            lapTime = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
            totalTime += lapTime;
            std::cerr << "Time: " << lapTime.count() << "ms / " << totalTime.count() << "ms\n";
            std::cout << coords << std::endl;
        }
    }

    return 0;
}
