#ifndef MONTECARLO_HPP
#define MONTECARLO_HPP

#include "bitutil.hpp"
#include "move.hpp"

#include <memory>

struct MonteCarloImpl;
struct RandomContext;

class MonteCarlo {
public:
    MonteCarlo(RandomContext & randCtx);
    Move bestMove(ull current, ull opponent, int moveNo);
    ~MonteCarlo();

private:
    std::unique_ptr<MonteCarloImpl> impl_;
};

#endif
