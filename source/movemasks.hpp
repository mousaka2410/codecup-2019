#ifndef MOVEMASKS_HPP
#define MOVEMASKS_HPP

#include <cstdint>
#include <cstdlib>
#include <algorithm>

using ull = std::uint64_t;

constexpr ull operator""_lsb_ull(const char * bits, std::size_t size) {
    std::size_t len = std::min(size, std::size_t{64});
    ull res = 0;
    for (std::size_t current = len; 0 < current; --current) {
        res <<= 1;
        res += bits[current - 1] - '0';
    }
    return res;
}

constexpr ull upperLeftMask44  = "11110000"
                                 "11110000"
                                 "11110000"
                                 "11110000"
                                 "00000000"
                                 "00000000"
                                 "00000000"
                                 "00000000"_lsb_ull;

constexpr ull upperRightMask44 = "00001111"
                                 "00001111"
                                 "00001111"
                                 "00001111"
                                 "00000000"
                                 "00000000"
                                 "00000000"
                                 "00000000"_lsb_ull;

constexpr ull lowerLeftMask44  = "00000000"
                                 "00000000"
                                 "00000000"
                                 "00000000"
                                 "11110000"
                                 "11110000"
                                 "11110000"
                                 "11110000"_lsb_ull;

constexpr ull lowerRightMask44 = "00000000" 
                                 "00000000"
                                 "00000000"
                                 "00000000"
                                 "00001111"
                                 "00001111"
                                 "00001111"
                                 "00001111"_lsb_ull;

constexpr ull upperLeftMask22  = "11001100"
                                 "11001100"
                                 "00000000"
                                 "00000000"
                                 "11001100"
                                 "11001100"
                                 "00000000"
                                 "00000000"_lsb_ull;

constexpr ull upperRightMask22 = "00110011"
                                 "00110011"
                                 "00000000"
                                 "00000000"
                                 "00110011"
                                 "00110011"
                                 "00000000"
                                 "00000000"_lsb_ull;

constexpr ull lowerLeftMask22  = "00000000"
                                 "00000000"
                                 "11001100"
                                 "11001100"
                                 "00000000"
                                 "00000000"
                                 "11001100"
                                 "11001100"_lsb_ull;

constexpr ull lowerRightMask22 = "00000000" 
                                 "00000000"
                                 "00110011"
                                 "00110011"
                                 "00000000"
                                 "00000000"
                                 "00110011"
                                 "00110011"_lsb_ull;

constexpr ull upperLeftMask11  = "10101010"
                                 "00000000"
                                 "10101010"
                                 "00000000"
                                 "10101010"
                                 "00000000"
                                 "10101010"
                                 "00000000"_lsb_ull;

constexpr ull upperRightMask11 = "01010101"
                                 "00000000"
                                 "01010101"
                                 "00000000"
                                 "01010101"
                                 "00000000"
                                 "01010101"
                                 "00000000"_lsb_ull;

constexpr ull lowerLeftMask11  = "00000000"
                                 "10101010"
                                 "00000000"
                                 "10101010"
                                 "00000000"
                                 "10101010"
                                 "00000000"
                                 "10101010"_lsb_ull;

constexpr ull lowerRightMask11 = "00000000" 
                                 "01010101"
                                 "00000000"
                                 "01010101"
                                 "00000000"
                                 "01010101"
                                 "00000000"
                                 "01010101"_lsb_ull;

constexpr ull transposeMask44 = upperLeftMask44 | lowerRightMask44;
constexpr ull transposeMask22 = upperLeftMask22 | lowerRightMask22;
constexpr ull transposeMask11 = upperLeftMask11 | lowerRightMask11;

constexpr ull horizontalFlipMask4 = "11110000"
                                    "11110000"
                                    "11110000"
                                    "11110000"
                                    "11110000"
                                    "11110000"
                                    "11110000"
                                    "11110000"_lsb_ull;

constexpr ull horizontalFlipMask2 = "11001100"
                                    "11001100"
                                    "11001100"
                                    "11001100"
                                    "11001100"
                                    "11001100"
                                    "11001100"
                                    "11001100"_lsb_ull;

constexpr ull horizontalFlipMask1 = "10101010"
                                    "10101010"
                                    "10101010"
                                    "10101010"
                                    "10101010"
                                    "10101010"
                                    "10101010"
                                    "10101010"_lsb_ull;


constexpr ull rayMask = "01111111"
                        "01000000"
                        "00100000"
                        "00010000"
                        "00001000"
                        "00000100"
                        "00000010"
                        "00000001"_lsb_ull;

constexpr ull majorDiagonalRayMask = "00000000"
                                     "01000000"
                                     "00100000"
                                     "00010000"
                                     "00001000"
                                     "00000100"
                                     "00000010"
                                     "00000001"_lsb_ull;

constexpr ull minorDiagonalRayMask = "00000000"
                                     "00000001"
                                     "00000010"
                                     "00000100"
                                     "00001000"
                                     "00010000"
                                     "00100000"
                                     "01000000"_lsb_ull;

constexpr ull allOnes = "11111111"
                        "11111111"
                        "11111111"
                        "11111111"
                        "11111111"
                        "11111111"
                        "11111111"
                        "11111111"_lsb_ull;

constexpr ull leftZero[8] = {
    allOnes,

    "01111111"
    "01111111"
    "01111111"
    "01111111"
    "01111111"
    "01111111"
    "01111111"
    "01111111"_lsb_ull,

    "00111111"
    "00111111"
    "00111111"
    "00111111"
    "00111111"
    "00111111"
    "00111111"
    "00111111"_lsb_ull,

    "00011111"
    "00011111"
    "00011111"
    "00011111"
    "00011111"
    "00011111"
    "00011111"
    "00011111"_lsb_ull,

    "00001111"
    "00001111"
    "00001111"
    "00001111"
    "00001111"
    "00001111"
    "00001111"
    "00001111"_lsb_ull,

    "00000111"
    "00000111"
    "00000111"
    "00000111"
    "00000111"
    "00000111"
    "00000111"
    "00000111"_lsb_ull,
    
    "00000011"
    "00000011"
    "00000011"
    "00000011"
    "00000011"
    "00000011"
    "00000011"
    "00000011"_lsb_ull,

    "00000001"
    "00000001"
    "00000001"
    "00000001"
    "00000001"
    "00000001"
    "00000001"
    "00000001"_lsb_ull,
};

constexpr ull leftOne[8] = {
    ~leftZero[0], ~leftZero[1], ~leftZero[2], ~leftZero[3],
    ~leftZero[4], ~leftZero[5], ~leftZero[6], ~leftZero[7],
};

constexpr ull rightZero[8] = {
    allOnes,

    "11111110"
    "11111110"
    "11111110"
    "11111110"
    "11111110"
    "11111110"
    "11111110"
    "11111110"_lsb_ull,

    "11111100"
    "11111100"
    "11111100"
    "11111100"
    "11111100"
    "11111100"
    "11111100"
    "11111100"_lsb_ull,

    "11111000"
    "11111000"
    "11111000"
    "11111000"
    "11111000"
    "11111000"
    "11111000"
    "11111000"_lsb_ull,

    "11110000"
    "11110000"
    "11110000"
    "11110000"
    "11110000"
    "11110000"
    "11110000"
    "11110000"_lsb_ull,

    "11100000"
    "11100000"
    "11100000"
    "11100000"
    "11100000"
    "11100000"
    "11100000"
    "11100000"_lsb_ull,

    "11000000"
    "11000000"
    "11000000"
    "11000000"
    "11000000"
    "11000000"
    "11000000"
    "11000000"_lsb_ull,

    "10000000"
    "10000000"
    "10000000"
    "10000000"
    "10000000"
    "10000000"
    "10000000"
    "10000000"_lsb_ull,
}; 

constexpr ull rightOne[8] = {
    ~rightZero[0], ~rightZero[1], ~rightZero[2], ~rightZero[3],
    ~rightZero[4], ~rightZero[5], ~rightZero[6], ~rightZero[7],
};

constexpr ull leftBitsMask = leftOne[1];
constexpr ull rightBitsMask = rightOne[1];

constexpr ull upperZero[8] = {
    allOnes,

    "00000000"
    "11111111"
    "11111111"
    "11111111"
    "11111111"
    "11111111"
    "11111111"
    "11111111"_lsb_ull,

    "00000000"
    "00000000"
    "11111111"
    "11111111"
    "11111111"
    "11111111"
    "11111111"
    "11111111"_lsb_ull,

    "00000000"
    "00000000"
    "00000000"
    "11111111"
    "11111111"
    "11111111"
    "11111111"
    "11111111"_lsb_ull,

    "00000000"
    "00000000"
    "00000000"
    "00000000"
    "11111111"
    "11111111"
    "11111111"
    "11111111"_lsb_ull,

    "00000000"
    "00000000"
    "00000000"
    "00000000"
    "00000000"
    "11111111"
    "11111111"
    "11111111"_lsb_ull,

    "00000000"
    "00000000"
    "00000000"
    "00000000"
    "00000000"
    "00000000"
    "11111111"
    "11111111"_lsb_ull,

    "00000000"
    "00000000"
    "00000000"
    "00000000"
    "00000000"
    "00000000"
    "11111111"_lsb_ull,
};

constexpr ull upperOne[8] {
    ~upperZero[0], ~upperZero[1], ~upperZero[2], ~upperZero[3],
    ~upperZero[4], ~upperZero[5], ~upperZero[6], ~upperZero[7],
};

constexpr ull upperBitsMask1 = upperOne[1];
constexpr ull upperBitsMask2 = upperOne[2];
constexpr ull lowerBitsMask1 = upperOne[7];

constexpr ull startPosWhite =  "00000000"
                               "00000000"
                               "00000000"
                               "00010000"
                               "00001000"
                               "00000000"
                               "00000000"
                               "00000000"_lsb_ull;

constexpr ull startPosBlack =  "00000000"
                               "00000000"
                               "00000000"
                               "00001000"
                               "00010000"
                               "00000000"
                               "00000000"
                               "00000000"_lsb_ull;

#endif
