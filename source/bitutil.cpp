#include "bitutil.hpp"

#include <ostream>

std::ostream & printBitMatrix(std::ostream & stream, ull matrix) {
    for (int i = 0; i < 64; ++i) {
        stream << (matrix & 1);
        matrix >>= 1;
        if ((i + 1) % 8 == 0)
            stream << '\n';
    }
    return stream;
}
