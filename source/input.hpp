#ifndef INPUT_HPP
#define INPUT_HPP

#include <string>
#include <iosfwd>

int coordsToMove(const std::string & coords);
std::string moveToCoords(int move);

bool isStarting(std::istream & stream);

#endif // INPUT_HPP
