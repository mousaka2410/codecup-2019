#ifndef BITUTIL_HPP
#define BITUTIL_HPP

#include "movemasks.hpp"

#include <iosfwd>

std::ostream & printBitMatrix(std::ostream & stream, ull matrix);

inline ull pointMirrorBitMatrix(ull matrix) {
    matrix = ((matrix & upperLeftMask44) << 36) |
             ((matrix >> 36) & upperLeftMask44) |
             ((matrix & upperRightMask44) << 28) |
             ((matrix >> 28) & upperRightMask44);

    matrix = ((matrix & upperLeftMask22) << 18) |
             ((matrix >> 18) & upperLeftMask22) |
             ((matrix & upperRightMask22) << 14) |
             ((matrix >> 14) & upperRightMask22);

    matrix = ((matrix & upperLeftMask11) << 9) |
             ((matrix >> 9) & upperLeftMask11) |
             ((matrix & upperRightMask11) << 7) |
             ((matrix >> 7) & upperRightMask11);

    return matrix;
}

inline ull transposeBitMatrix(ull matrix) {
    matrix = (matrix & transposeMask44) |
             ((matrix & upperRightMask44) << 28) |
             ((matrix >> 28) & upperRightMask44);

    matrix = (matrix & transposeMask22) |
             ((matrix & upperRightMask22) << 14) |
             ((matrix >> 14) & upperRightMask22);

    matrix = (matrix & transposeMask11) |
             ((matrix & upperRightMask11) << 7) |
             ((matrix >> 7) & upperRightMask11);

    return matrix;
}

inline ull rotateBitMatrixRight(ull matrix) {
    matrix = ((matrix & upperLeftMask44) << 4) |
             ((matrix & upperRightMask44) << 32) |
             ((matrix & lowerLeftMask44) >> 32) |
             ((matrix & lowerRightMask44) >> 4);

    matrix = ((matrix & upperLeftMask22) << 2) |
             ((matrix & upperRightMask22) << 16) |
             ((matrix & lowerLeftMask22) >> 16) |
             ((matrix & lowerRightMask22) >> 2);

    matrix = ((matrix & upperLeftMask11) << 1) |
             ((matrix & upperRightMask11) << 8) |
             ((matrix & lowerLeftMask11) >> 8) |
             ((matrix & lowerRightMask11) >> 1);

    return matrix;
}

inline ull rotateBitMatrixLeft(ull matrix) {
    matrix = ((matrix & upperLeftMask44) << 32) |
             ((matrix & upperRightMask44) >> 4) |
             ((matrix & lowerLeftMask44) << 4) |
             ((matrix & lowerRightMask44) >> 32);

    matrix = ((matrix & upperLeftMask22) << 16) |
             ((matrix & upperRightMask22) >> 2) |
             ((matrix & lowerLeftMask22) << 2) |
             ((matrix & lowerRightMask22) >> 16);

    matrix = ((matrix & upperLeftMask11) << 8) |
             ((matrix & upperRightMask11) >> 1) |
             ((matrix & lowerLeftMask11) << 1) |
             ((matrix & lowerRightMask11) >> 8);

    return matrix;
}

inline ull horizontalFlipBitMatrix(ull matrix) {
    matrix = ((matrix & horizontalFlipMask4) << 4) |
             ((matrix >> 4) & horizontalFlipMask4);

    matrix = ((matrix & horizontalFlipMask2) << 2) |
             ((matrix >> 2) & horizontalFlipMask2);

    matrix = ((matrix & horizontalFlipMask1) << 1) |
             ((matrix >> 1) & horizontalFlipMask1);

    return matrix;
}

inline ull leftRot8(ull bits) {
    return (bits << 8) | (bits >> 56);
}

inline ull rotateBitsRight(ull bits, int pos) {
    return (bits >> pos) | (bits << (64 - pos));
}

inline ull rotateBitsLeft(ull bits, int pos) {
    return (bits << pos) | (bits >> (64 - pos));
}

inline int popcnt(ull bits) {
    bits = (bits & 0x5555555555555555) + ((bits >>  1) & 0x5555555555555555);
    bits = (bits & 0x3333333333333333) + ((bits >>  2) & 0x3333333333333333);
    bits = (bits & 0x0F0F0F0F0F0F0F0F) + ((bits >>  4) & 0x0F0F0F0F0F0F0F0F);
    bits = (bits & 0x00FF00FF00FF00FF) + ((bits >>  8) & 0x00FF00FF00FF00FF);
    bits = (bits & 0x0000FFFF0000FFFF) + ((bits >> 16) & 0x0000FFFF0000FFFF);
    bits = (bits & 0x00000000FFFFFFFF) + ((bits >> 32) & 0x00000000FFFFFFFF);
    return static_cast<int>(bits);
}

inline int indexOfSingleBit(ull singleBit) {
    return popcnt(singleBit - 1);
}

#endif
