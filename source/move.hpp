#ifndef MOVE_HPP
#define MOVE_HPP

#include "bitutil.hpp"
#include <algorithm>
#include <limits>

ull moveFlipMask(ull currentPlayersPieces, ull opponentsPieces, int pos);
ull moveFlipMaskV2(ull currentPlayersPieces, ull opponentsPieces, int pos);
ull moveFlipMask_branched(ull currentPlayersPieces, ull opponentsPieces, int pos);

ull adjacentsMask(ull currentPlayersPieces, ull opponentsPieces);
ull possibleMovesMask(ull currentPlayersPieces, ull opponentsPieces);

template <class ContextType, void (*func)(ContextType & context, ull currentPlayersPieces, ull opponentsPieces, int pos)>
void visitPossibleMoves(ContextType & context, ull currentPlayersPieces, ull opponentsPieces) {
    ull moves = possibleMovesMask(currentPlayersPieces, opponentsPieces);
    while (moves) {
        ull intermediate = moves - 1;
        int pos = popcnt(intermediate & ~moves);
        moves = intermediate & moves;
        func(context, currentPlayersPieces, opponentsPieces, pos);
    }
}

template <class ContextType, void (*func)(ContextType & context, ull currentPlayersPieces, ull opponentsPieces, int pos, int moveNo)>
void visitPossibleMoves(ContextType & context, ull currentPlayersPieces, ull opponentsPieces, int moveNo) {
    ull moves = possibleMovesMask(currentPlayersPieces, opponentsPieces);
    while (moves) {
        ull intermediate = moves - 1;
        int pos = popcnt(intermediate & ~moves);
        moves = intermediate & moves;
        func(context, currentPlayersPieces, opponentsPieces, pos, moveNo);
    }
}

inline void addPiece(ull & position, int move) {
    position |= (1ULL << move);
}

inline void applyFlip(ull & oneColor, ull & otherColor, ull flipMask) {
    oneColor ^= flipMask;
    otherColor ^= flipMask;
}

struct Move {
    int move = -1;
    int score = std::numeric_limits<int>::min();
    ull flipMask = 0ULL;
    Move() = default;
    Move(int pos, ull flipMask, int score = std::numeric_limits<int>::min())
        : move(pos), score(score), flipMask(flipMask) {;}
};

struct Position {
    ull current;
    ull opponent;
    Position(ull current, ull opponent) : current(current), opponent(opponent) {;}
};

inline void applyMoveAndSwap(Position & position, const Move & move) {
    applyFlip(position.current, position.opponent, move.flipMask);
    addPiece(position.current, move.move);
    std::swap(position.current, position.opponent);
}


inline void applyMoveAndSwap(Position & position, const Move & move, int & moveNo) {
    applyMoveAndSwap(position, move);
    moveNo++;
}

inline Move getMove(const Position & position, int pos) {
    Move move;
    move.move = pos;
    move.flipMask = moveFlipMaskV2(position.current, position.opponent, pos);
    return move;
}

inline int scoreForCurrent(const Position & position) {
    return popcnt(position.current) - 2;
}

#endif
