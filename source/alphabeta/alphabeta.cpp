#include "alphabeta.hpp"

#include <limits>
#include <vector>
#include <cassert>

struct AlphaBetaFrame {
    int depth;
    int alpha;
    int beta;
    bool maximizingPlayer;
    Move bestMove;
    AlphaBetaFrame(int depth, int alpha, int beta, bool maximizingPlayer)
        : depth(depth), alpha(alpha), beta(beta), maximizingPlayer(maximizingPlayer)
    { ; }
};

int alphabeta(const AlphaBetaFrame & frame, const Position & position);

void doPlayerDependentStuff(AlphaBetaFrame & frame, const Move & move) {
    if (frame.maximizingPlayer) {
        if (move.score > frame.bestMove.score)
            frame.bestMove = move;
        frame.alpha = std::max(frame.alpha, move.score);
    } else {
        if (move.score < frame.bestMove.score)
            frame.bestMove = move;
        frame.alpha = std::min(frame.beta, move.score);
    }
}

void visitPossibleMoves(AlphaBetaFrame & frame, const Position & position) {
    ull moves = possibleMovesMask(position.current, position.opponent);
    ull tryAgainMoves = moves;
    Move bestMove;
    bool tryAgain = true;
    while (moves) {
        ull intermediate = moves - 1;
        int squareNo = popcnt(intermediate & ~moves);
        moves = intermediate & moves;
        Move move = getMove(position, squareNo);
        if (move.flipMask) {
            tryAgain = false;
            Position newPosition = position;
            applyMoveAndSwap(newPosition, move);
            move.score = alphabeta(frame, newPosition);
            doPlayerDependentStuff(frame, move);
            if (frame.alpha >= frame.beta)
                return;
        }
    }
    if (tryAgain) {
        moves = tryAgainMoves;
        while (moves) {
            Move move;
            ull intermediate = moves - 1;
            move.move = popcnt(intermediate & ~moves);
            moves = intermediate & moves;
            Position newPosition = { position.opponent, position.current };
            addPiece(newPosition.opponent, move.move);
            move.score = alphabeta(frame, newPosition);
            doPlayerDependentStuff(frame, move);
            if (frame.alpha >= frame.beta)
                return;
        }
    }
}

int alphabeta(const AlphaBetaFrame & frame, const Position & position) {
    if (frame.depth == 0) {
        int score = scoreForCurrent(position);
        return frame.maximizingPlayer ? score : 60 - score;
    }
    AlphaBetaFrame nextFrame(frame.depth - 1, frame.alpha, frame.beta, !frame.maximizingPlayer);
    nextFrame.bestMove.score = nextFrame.maximizingPlayer ? std::numeric_limits<int>::min() : std::numeric_limits<int>::max();
    visitPossibleMoves(nextFrame, position);
    return nextFrame.bestMove.score;
}

Move alphabeta(const Position & position, int depth, bool maximizingPlayer) {
    assert(depth != 0);
    AlphaBetaFrame frame(depth - 1, std::numeric_limits<int>::min(), std::numeric_limits<int>::max(), maximizingPlayer);
    frame.bestMove.score = frame.maximizingPlayer ? std::numeric_limits<int>::min() : std::numeric_limits<int>::max();
    visitPossibleMoves(frame, position);
    return frame.bestMove;
}
