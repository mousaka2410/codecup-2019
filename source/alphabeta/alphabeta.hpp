#ifndef ALPHABETA_HPP
#define ALPHABETA_HPP

#include "move.hpp"

Move alphabeta(const Position & position, int depth, bool maximizingPlayer = true);

#endif
