#!/bin/sh

cat \
    ../../../source/movemasks.hpp \
    ../../../source/bitutil.hpp \
    ../../../source/move.hpp \
    ../../../source/input.hpp \
    ../../../source/bitutil.cpp \
    ../../../source/move.cpp \
    ../../../source/input.cpp \
    ../../../source/montecarlo/montecarlo.hpp \
    ../../../source/montecarlo/montecarlo.cpp \
    ../../../source/montecarlo/dantes.cpp \
    | \
    grep -v '#include "' \
    > \
    dantes_submission.cpp
    
