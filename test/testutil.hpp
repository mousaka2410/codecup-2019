#ifndef TESTUTIL_HPP
#define TESTUTIL_HPP

#include "bitutil.hpp"

#include <ostream>

struct Mask {
    ull mask;
};

bool operator==(const Mask & lhs, const Mask & rhs) {
    return lhs.mask == rhs.mask;
}

std::ostream & operator<<(std::ostream & stream, const Mask & mask) {
    stream << '\n';
    printBitMatrix(stream, mask.mask);
    return stream;
}

#endif
