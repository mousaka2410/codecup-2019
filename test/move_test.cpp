#include "testutil.hpp"

#include "bitutil.hpp"
#include "move.hpp"

#include <gtest/gtest.h>

#include <set>

namespace {
constexpr ull currentPlayersPieces = "10000111"
                                     "00100101"
                                     "00000101"
                                     "10011110"
                                     "00101001"
                                     "00101001"
                                     "01001101"
                                     "11111101"_lsb_ull;

constexpr ull opponentPieces = "01000000"
                               "11000010"
                               "10111000"
                               "00100001"
                               "11010110"
                               "00010100"
                               "00010010"
                               "00000010"_lsb_ull;

constexpr ull expectedFlipMask = "00000000"
                                 "00000110"
                                 "00001100"
                                 "00001010"
                                 "00001000"
                                 "00001000"
                                 "00001000"
                                 "00000000"_lsb_ull;

constexpr ull bitMatrix = currentPlayersPieces;

constexpr ull bitMatrixRotatedLeft = "11101111"
                                     "10010000"
                                     "11110011"
                                     "00011111"
                                     "00010001"
                                     "01001101"
                                     "00000011"
                                     "10010001"_lsb_ull;

constexpr ull bitMatrixPointMirrored = "10111111"
                                       "10110010"
                                       "10010100"
                                       "10010100"
                                       "01111001"
                                       "10100000"
                                       "10100100"
                                       "11100001"_lsb_ull;

constexpr ull bitMatrixRotatedRight = "10001001"
                                      "11000000"
                                      "10110010"
                                      "10001000"
                                      "11111000"
                                      "11001111"
                                      "00001001"
                                      "11110111"_lsb_ull;

constexpr ull bitMatrixLeftRot8 = "11111101"
                                  "10000111"
                                  "00100101"
                                  "00000101"
                                  "10011110"
                                  "00101001"
                                  "00101001"
                                  "01001101"_lsb_ull;

constexpr ull testPositionBlack0 = "01111110"
                                   "00111111"
                                   "01011111"
                                   "01101111"
                                   "01111111"
                                   "01111111"
                                   "01111111"
                                   "01111110"_lsb_ull;

constexpr ull testPositionWhite0 = "10000000"
                                   "11000000"
                                   "10100000"
                                   "10010000"
                                   "10000000"
                                   "10000000"
                                   "10000000"
                                   "10000000"_lsb_ull;

constexpr int testPositionMove0 = 63;


constexpr ull testPositionBlack1 = "00011100"
                                   "00000000"
                                   "00010000"
                                   "00001000"
                                   "00010000"
                                   "00001000"
                                   "00000000"
                                   "00000000"_lsb_ull;

constexpr ull testPositionWhite1 = "00000000"
                                   "00010000"
                                   "00100000"
                                   "00110000"
                                   "01001000"
                                   "00000100"
                                   "00000000"
                                   "00000000"_lsb_ull;

constexpr ull adjacentsPos1  = "00100010"
                               "01101110"
                               "01001100"
                               "11000100"
                               "10100110"
                               "11110010"
                               "00011110"
                               "00000000"_lsb_ull;

constexpr ull possibleMovesPos1 = "00100010"
                                  "01101000"
                                  "01001100"
                                  "01000100"
                                  "00100100"
                                  "10110010"
                                  "00001110"
                                  "00000000"_lsb_ull;

inline void saveVisitedMove(std::multiset<int> & visitedMoves, ull, ull, int move) {
    visitedMoves.insert(move);
}

const std::multiset<int> movesToVisitPos1 = { 2, 6, 9, 10, 12, 17, 20, 21, 25, 29, 34, 37, 40, 42, 43, 46, 52, 53, 54 };

}

TEST(MoveTest, BitMatrixRotations) {
    EXPECT_EQ(Mask{rotateBitMatrixLeft(bitMatrix)}, Mask{bitMatrixRotatedLeft});
    EXPECT_EQ(Mask{rotateBitMatrixRight(bitMatrix)}, Mask{bitMatrixRotatedRight});
    EXPECT_EQ(Mask{pointMirrorBitMatrix(bitMatrix)}, Mask{bitMatrixPointMirrored});

    EXPECT_EQ(Mask{rotateBitMatrixLeft(rotateBitMatrixRight(bitMatrix))}, Mask{bitMatrix});
    EXPECT_EQ(Mask{rotateBitMatrixRight(rotateBitMatrixLeft(bitMatrix))}, Mask{bitMatrix});
    EXPECT_EQ(Mask{pointMirrorBitMatrix(pointMirrorBitMatrix(bitMatrix))}, Mask{bitMatrix});
}

TEST(MoveTest, LeftRot8) {
    EXPECT_EQ(Mask{leftRot8(bitMatrix)}, Mask{bitMatrixLeftRot8});
    EXPECT_EQ(Mask{leftRot8(leftRot8(leftRot8(leftRot8(leftRot8(leftRot8(leftRot8(leftRot8(bitMatrix))))))))}, Mask{bitMatrix});
}

TEST(MoveTest, FlipsCorrectPieces) {
    ull flippingPieces = moveFlipMask(currentPlayersPieces, opponentPieces, 12);
    ASSERT_EQ(Mask{expectedFlipMask}, Mask{flippingPieces});
}

TEST(MoveTest, FlipsCorrectPieces_branched) {
    ull flippingPieces = moveFlipMask_branched(currentPlayersPieces, opponentPieces, 12);
    ASSERT_EQ(Mask{expectedFlipMask}, Mask{flippingPieces});
}

TEST(MoveTest, FlipsCorrectPiecesV2) {
    ull flippingPieces = moveFlipMaskV2(currentPlayersPieces, opponentPieces, 12);
    ASSERT_EQ(Mask{expectedFlipMask}, Mask{flippingPieces});
}

TEST(MoveTest, FlipsCorrectPiecesPos0) {
    ull flippingPieces = moveFlipMask(testPositionWhite0, testPositionBlack0, testPositionMove0);
    ull flippingPiecesV2 = moveFlipMaskV2(testPositionWhite0, testPositionBlack0, testPositionMove0);
    ull flippingPieces_branched = moveFlipMask_branched(testPositionWhite0, testPositionBlack0, testPositionMove0);
    EXPECT_EQ(Mask{flippingPiecesV2}, Mask{flippingPieces});
    EXPECT_EQ(Mask{flippingPiecesV2}, Mask{flippingPieces_branched});
}

TEST(PossibleMoves, AdjacentsPos1) {
    ull adjacents = adjacentsMask(testPositionBlack1, testPositionWhite1);
    EXPECT_EQ(Mask{adjacents}, Mask{adjacentsPos1});
}

TEST(PossibleMoves, PossibleMovesPos1) {
    ull moves = possibleMovesMask(testPositionBlack1, testPositionWhite1);
    EXPECT_EQ(Mask{moves}, Mask{possibleMovesPos1});
}

TEST(PossibleMoves, VisitsPossibleMoves) {
    std::multiset<int> visitedMoves;
    visitPossibleMoves<std::multiset<int>, saveVisitedMove>(visitedMoves, testPositionBlack1, testPositionWhite1);
    EXPECT_EQ(visitedMoves, movesToVisitPos1);
}