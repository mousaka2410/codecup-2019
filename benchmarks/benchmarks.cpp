#include <move.hpp>
#include <benchmark/benchmark.h>
#include <limits>

namespace {
constexpr ull currentPlayersPieces = "10000111"
                                     "00100101"
                                     "00000101"
                                     "10011110"
                                     "00101001"
                                     "00101001"
                                     "01001101"
                                     "11111101"_lsb_ull;

constexpr ull opponentPieces = "01000000"
                               "11000010"
                               "10111000"
                               "00100001"
                               "11010110"
                               "00010100"
                               "00010010"
                               "00000010"_lsb_ull;

constexpr ull expectedFlipMask = "00000000"
                                 "00000110"
                                 "00001100"
                                 "00001010"
                                 "00001000"
                                 "00001000"
                                 "00001000"
                                 "00000000"_lsb_ull;


constexpr ull testPositionBlack1 = "00011100"
                                   "00000000"
                                   "00010000"
                                   "00001000"
                                   "00010000"
                                   "00001000"
                                   "00000000"
                                   "00000000"_lsb_ull;

constexpr ull testPositionWhite1 = "00000000"
                                   "00010000"
                                   "00100000"
                                   "00110000"
                                   "01001000"
                                   "00000100"
                                   "00000000"
                                   "00000000"_lsb_ull;

constexpr ull adjacentsPos1  = "00100010"
                               "01101110"
                               "01001100"
                               "11000100"
                               "10100110"
                               "11110010"
                               "00011110"
                               "00000000"_lsb_ull;

constexpr ull possibleMovesPos1 = "00100010"
                                  "01101000"
                                  "01001100"
                                  "01000100"
                                  "00100100"
                                  "10110010"
                                  "00001110"
                                  "00000000"_lsb_ull;

void countMoves(int & count, ull, ull, int) {
    count++;
}

struct MoveStats {
    int move = -1;
    int score = std::numeric_limits<int>::min();
    ull flipMask = 0ULL;
};

void findGreediestMove(MoveStats & stats, ull currentPlayersPieces, ull opponentsPieces, int move) {
    ull flipMask = moveFlipMaskV2(currentPlayersPieces, opponentsPieces, move);
    int nCurrentsPieces = popcnt(currentPlayersPieces ^ flipMask) + 1;
    if (stats.score < nCurrentsPieces) {
        stats.move = move;
        stats.score = nCurrentsPieces;
        stats.flipMask = flipMask;
    }
}

}// unnamed ns

static void BM_moveFlipMask(benchmark::State& state) {
    for (auto _ : state) {
        moveFlipMask(currentPlayersPieces, opponentPieces, 12);
    }
}
BENCHMARK(BM_moveFlipMask);

static void BM_moveFlipMaskV2(benchmark::State& state) {
    for (auto _ : state) {
        moveFlipMaskV2(currentPlayersPieces, opponentPieces, 12);
    }
}
BENCHMARK(BM_moveFlipMaskV2);

static void BM_moveFlipMask_branched(benchmark::State& state) {
    for (auto _ : state) {
        moveFlipMask_branched(currentPlayersPieces, opponentPieces, 12);
    }
}
BENCHMARK(BM_moveFlipMask_branched);

static void BM_moveFlipMask_startPos(benchmark::State& state) {
    for (auto _ : state) {
        moveFlipMask(startPosWhite, startPosBlack, 19);
    }
}
BENCHMARK(BM_moveFlipMask_startPos);

static void BM_moveFlipMaskV2_startPos(benchmark::State& state) {
    for (auto _ : state) {
        moveFlipMaskV2(startPosWhite, startPosBlack, 19);
    }
}
BENCHMARK(BM_moveFlipMaskV2_startPos);

static void BM_moveFlipMask_branched_startPos(benchmark::State& state) {
    for (auto _ : state) {
        moveFlipMask_branched(startPosWhite, startPosBlack, 19);
    }
}
BENCHMARK(BM_moveFlipMask_branched_startPos);

static void BM_visitsMoves(benchmark::State& state) {
    for (auto _ : state) {
        int count = 0;
        visitPossibleMoves<int, countMoves>(count, testPositionBlack1, testPositionWhite1);
    }
}
BENCHMARK(BM_visitsMoves);

static void BM_findGreediestMove(benchmark::State& state) {
    for (auto _ : state) {
        MoveStats bestStats;
        visitPossibleMoves<MoveStats, findGreediestMove>(bestStats, testPositionBlack1, testPositionWhite1);
    }
}
BENCHMARK(BM_findGreediestMove);